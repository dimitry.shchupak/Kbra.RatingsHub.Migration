namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;

public interface IApplicationService
{
    Task Run(string[] args);
}