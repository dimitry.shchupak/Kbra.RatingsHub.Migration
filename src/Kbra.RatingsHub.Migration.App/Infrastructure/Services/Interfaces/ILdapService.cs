using Kbra.RatingsHub.Migration.App.Domain.Entities;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;

public interface ILdapService
{
    List<UserDetail> GetUsers(IEnumerable<string> emails, bool disabled = false);
}