using Kbra.RatingsHub.Migration.App.Domain.Entities;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;

public interface IUserService
{
    Task<List<User>> GetUsers(IEnumerable<int> ids);
}