using Kbra.RatingsHub.Migration.App.Domain.Entities;
using Kbra.RatingsHub.Migration.App.Extensions;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Abstract;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;
using Microsoft.Extensions.Logging;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services;

public sealed class UserService : ServiceBase, IUserService
{
    #region Constructors

    public UserService(ILogger<UserService> logger, IHttpClientFactory httpClientFactory) : base(logger,
        httpClientFactory, "UserClient")
    {
    }

    #endregion


    #region Public Methods

    public async Task<List<User>> GetUsers(IEnumerable<int> ids)
    {
        const string companyId = "111"; // Company id for KBRA employees

        var request = new HttpRequest
        {
            Parameters = new List<KeyValuePair<string, string?>>
            {
                new("companyIds", companyId),
                new("ids", string.Join(",", ids))
            },
            RequestUri = "users"
        };

        var response = await GetAsync(request);

        if (!response.IsSuccessStatusCode)
        {
            Logger.LogDebug("Users API request failed: {ResponseStatusCode}, {ResponseContent}", response.StatusCode,
                response.Content);

            return new List<User>();
        }

        var model = response.Content.Deserialize<UserServiceResponse<List<User>>>();

        var users = model?.Data ?? new List<User>();

        return users;
    }

    #endregion
}