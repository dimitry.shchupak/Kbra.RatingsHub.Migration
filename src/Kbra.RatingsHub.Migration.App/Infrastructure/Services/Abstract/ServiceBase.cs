using System.Net.Mime;
using System.Text;
using System.Xml.Linq;
using Kbra.RatingsHub.Migration.App.Extensions;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;
using Mapster;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Abstract;

public abstract class ServiceBase
{
    #region Private Fields

    private readonly HttpClient _httpClient;

    #endregion


    #region Protected Fields

    protected readonly ILogger<ServiceBase> Logger;

    #endregion


    #region Constructors

    protected ServiceBase(ILogger<ServiceBase> logger, IHttpClientFactory httpClientFactory, string httpClientName)
    {
        Logger = logger ?? throw new ArgumentNullException(nameof(logger));

        if (httpClientFactory == null)
            throw new ArgumentNullException(nameof(httpClientFactory));

        if (httpClientName == null)
            throw new ArgumentNullException(nameof(httpClientName));

        _httpClient = httpClientFactory.CreateClient(httpClientName);
    }

    #endregion


    #region Protected Methods

    protected Task<HttpResponse> DeleteAsync(HttpRequest request) => GetResponseAsync(HttpMethod.Delete, request.Adapt<HttpRequest<object>>());

    protected Task<HttpResponse> GetAsync(HttpRequest request) => GetResponseAsync(HttpMethod.Get, request.Adapt<HttpRequest<object>>());

    protected Task<HttpResponse> PatchAsync<T>(HttpRequest<T> request) => GetResponseAsync(HttpMethod.Patch, request);

    protected Task<HttpResponse> PostAsync(HttpRequest request) => GetResponseAsync(HttpMethod.Post, request.Adapt<HttpRequest<object>>());

    protected Task<HttpResponse> PostAsync<T>(HttpRequest<T> request) => GetResponseAsync(HttpMethod.Post, request);

    #endregion


    #region Private Methods

    private static HttpContent? GetHttpContent(object? data)
    {
        if (data == null)
            return null;

        return data switch
        {
            HttpContent content => content,
            List<KeyValuePair<string, string?>> dictionary => GetUrlEncodedContent(dictionary),
            XDocument document => new StringContent(document.ToString(), Encoding.UTF8, MediaTypeNames.Text.Xml),
            _ => GetJsonContent(data)
        };
    }

    private static StringContent GetJsonContent(object data)
    {
        var json = data.Serialize(false);

        return new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);
    }

    private async Task<HttpResponse> GetResponseAsync<T>(HttpMethod method, HttpRequest<T> request)
    {
        var requestUri = GetRequestUri(request.RequestUri, request.Parameters);
        using var content = GetHttpContent(request.Content);

        using var requestMessage = new HttpRequestMessage(method, requestUri)
        {
            Content = content
        };

        if (request.Headers != null && request.Headers.Any())
            foreach (var header in request.Headers)
                requestMessage.Headers.Add(header.Key, header.Value);

        using var response = await _httpClient.SendAsync(requestMessage);

        return new HttpResponse
        {
            Content = await response.Content.ReadAsStringAsync(),
            Headers = response.Headers.ToDictionary(x => x.Key, x => x.Value),
            StatusCode = response.StatusCode
        };
    }

    private static string GetRequestUri(string requestUri, IEnumerable<KeyValuePair<string, string?>>? parameters) =>
        parameters == null ? requestUri : QueryHelpers.AddQueryString(requestUri, parameters);

    private static FormUrlEncodedContent GetUrlEncodedContent(IEnumerable<KeyValuePair<string, string?>> parameters) => new(parameters);

    #endregion
}