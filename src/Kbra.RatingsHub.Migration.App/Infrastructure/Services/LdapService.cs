using Kbra.RatingsHub.Migration.App.Domain.Entities;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;
using Novell.Directory.Ldap;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services;

public sealed class LdapService : ILdapService
{
    #region Private Fields

    private readonly ILdapConnection _ldapConnection;

    #endregion


    #region Constructors

    public LdapService(string username, string password)
    {
        _ldapConnection = new LdapConnection();
        
        _ldapConnection.Connect("kbra-dc1.kbra.network", 389);
        _ldapConnection.Bind(username, password);
    }

    #endregion


    #region Public Methods

    public List<UserDetail> GetUsers(IEnumerable<string> emails, bool disabled = false)
    {
        var baseDn = $"OU=KBRA_{(disabled ? "Disabled" : "Users")},dc=kbra,dc=local";
        var query = emails.Aggregate(string.Empty, (current, next) => $"{current}(mail={next})");
        var results = _ldapConnection.Search(baseDn, LdapConnection.ScopeSub,
            $"(&(objectCategory=User)(|{query}))",
            new[]
            {
                "department",
                "mail"
            }, false);
        
        return results.Select(x => new UserDetail
        {
            BusinessUnit = x.GetAttribute("department").StringValue,
            Email = x.GetAttribute("mail").StringValue
        }).ToList();
    }

    #endregion
}