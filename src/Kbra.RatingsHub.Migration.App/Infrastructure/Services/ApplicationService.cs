using System.Globalization;
using CsvHelper;
using Kbra.RatingsHub.Migration.App.Domain.Entities.RatingsHub;
using Kbra.RatingsHub.Migration.App.Extensions;
using Kbra.RatingsHub.Migration.App.Infrastructure.Persistence;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SearchAThing.EFUtil;
using Action = Kbra.RatingsHub.Migration.App.Enumerations.Action;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services;

public sealed class ApplicationService : IApplicationService
{
    #region Private Fields

    private readonly ILogger<ApplicationService> _logger;

    private readonly RatingsDbContext _ratingsDbContext;

    private readonly RatingsHubDbContext _ratingsHubDbContext;

    private readonly ThunderballDbContext _thunderballDbContext;

    private readonly LdapService _ldapService;

    private readonly IUserService _userService;

    private readonly IDictionary<string, List<string>> _businessUnitMap = new Dictionary<string, List<string>>
    {
        {
            "ABS", new List<string>
            {
                "ABS Commercial",
                "ABS Consumer"
            }
        },
        {
            "CMBS", new List<string>
            {
                "CMBS Surveillance",
                "Co-Head CMBS Ratings"
            }
        },
        {
            "Financial Institutions", new List<string>
            {
                "Financial Institutions and SRS"
            }
        },
        {
            "Insurance", new List<string>
            {
                "Human Resources"
            }
        },
        {
            "Project Finance & Infrastructure", new List<string>
            {
                "European Head Project Finance & Infrastructure",
                "Project Finance",
                "Project Finance, Infrastructure & Corporates"
            }
        },
        {
            "Public Finance", new List<string>
            {
                "Public Finance / Financial Guarantors"
            }
        },
        {
            "Sovereigns", new List<string>
            {
                "Sales"
            }
        },
        {
            "Structured Credit", new List<string>
            {
                "Strategy"
            }
        },
        {
            "Transportation and Commercial Finance", new List<string>
            {
                "Aviation, Transportation and Commercial Finance",
                "Aviation, Transportation & Commercial Finance"
            }
        }
    };

    #endregion


    #region Constructors

    public ApplicationService(ILogger<ApplicationService> logger, RatingsDbContext ratingsDbContext,
        RatingsHubDbContext ratingsHubDbContext, ThunderballDbContext thunderballDbContext, LdapService ldapService,
        IUserService userService)
    {
        _logger = logger;
        _ratingsDbContext = ratingsDbContext;
        _ratingsHubDbContext = ratingsHubDbContext;
        _thunderballDbContext = thunderballDbContext;
        _ldapService = ldapService;
        _userService = userService;

        TypeAdapterConfig<Domain.Entities.Ratings.CreditEstimate, CreditEstimate>.NewConfig()
            //.Ignore(d => d.CreditEstimateId)
            .Map(d => d.AnalystId, s => s.CreatedByUserId)
            .Map(d => d.CompletedOnDate, s => s.CreditEstimateDate)
            .Map(d => d.CreditValueId, s => s.CreditEstimateScaleId)
            .AfterMapping((s, d) =>
            {
                d.RatingOutlookId ??= 0;

                if (s.UpdatedByUserId == null)
                    d.UpdatedByUserId = s.CreatedByUserId.GetValueOrDefault();
            });
    }

    #endregion


    #region Public Methods

    public async Task Run(string[] args)
    {
        _logger.LogInformation("Application Started");

        var action = GetAction(args);
        var businessUnits = _ratingsHubDbContext.BusinessUnits.ToList();
        var liborExposures = GetLiborExposures();
        var ratingsCreditEstimates = _ratingsDbContext.CreditEstimates.ToList();

        var users = await _userService.GetUsers(ratingsCreditEstimates.Where(x => x.CreatedByUserId != null)
            .Select(x => x.CreatedByUserId!.Value).Distinct());

        var userEmails = users.Select(x => x.Email).Distinct().ToList();
        var userActiveDetails = _ldapService.GetUsers(userEmails);
        var userDisabledDetails = _ldapService.GetUsers(userEmails, true);
        var userDetails = userActiveDetails.Union(userDisabledDetails).ToList();

        var userInfos = (from u in users
            join ud in userDetails on u.Email equals ud.Email into g
            from x in g.DefaultIfEmpty()
            select new UserInfo
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Email = u.Email,
                BusinessUnit = GetBusinessUnit(u.Id, x?.BusinessUnit?.Trim())
            }).ToList();

        var ratingsHubCreditEstimates = ratingsCreditEstimates.Adapt<List<CreditEstimate>>();

        foreach (var creditEstimate in ratingsHubCreditEstimates)
        {
            creditEstimate.BusinessUnitId =
                GetBusinessUnitId(businessUnits, userInfos, creditEstimate.AnalystId);
            creditEstimate.LiborExposure = GetLiborExposure(liborExposures, creditEstimate.CreatedByWorkflowId);
        }

        if (action == Action.Export)
            await Export(userInfos, ratingsHubCreditEstimates, businessUnits);
        else
            await Persist(ratingsHubCreditEstimates);

        _logger.LogInformation("Application Completed");
    }

    #endregion


    #region Private Methods

    private static async Task Export(string filename, IEnumerable<object> data)
    {
        await using var writer = new StreamWriter($"{Directory.GetCurrentDirectory()}/{filename}.csv");

        await using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);

        await csv.WriteRecordsAsync(data);
    }

    private async Task Export(IReadOnlyCollection<UserInfo> userInfos,
        IEnumerable<CreditEstimate> creditEstimates,
        IEnumerable<BusinessUnit> businessUnits)
    {
        await Export("Users", userInfos);

        var creditEstimateInfo = from ce in creditEstimates
            join b in businessUnits on ce.BusinessUnitId equals b.BusinessUnitId
            join u in userInfos on ce.AnalystId equals u.Id into g
            from x in g.DefaultIfEmpty()
            select new
            {
                ce.AnalystId,
                AnalystName = x == null ? string.Empty : $"{x.FirstName} {x.LastName}",
                AnalystEmail = x == null ? string.Empty : x.Email,
                AnalystBusinessUnit = x == null ? string.Empty : x.BusinessUnit,
                CreditEstimateBusinessUnitId = ce.BusinessUnitId,
                CreditEstimateBusinessUnitName = b.Name,
                ce.CompletedOnDate,
                ce.CreatedByUserId,
                ce.CreatedByWorkflowId,
                ce.CreditEstimateId,
                ce.CreditValueId,
                ce.EntityId,
                ce.IsActive,
                ce.IsCorrection,
                ce.IsExpired,
                ce.LiborExposure,
                ce.RatingOutlookId,
                ce.UpdatedByUserId
            };

        await Export("CreditEstimates", creditEstimateInfo);

        _logger.LogInformation("Export Completed");
    }

    private static Action GetAction()
    {
        Console.Error.WriteLine("Action is not valid.");

        Action? action;

        while (true)
        {
            Console.WriteLine("Which action would you like to perform?");
            Console.WriteLine("1. Export");
            Console.WriteLine("2. Persist");

            var input = Console.ReadLine();

            if (input?.Trim().Equals("Q", StringComparison.OrdinalIgnoreCase) == true)
                Environment.Exit(0);

            action = input?.ParseAction();

            if (action != null && action != Action.Unknown)
                break;

            Console.Error.WriteLine("Action is not valid.");
        }

        return action.Value;
    }

    private static Action GetAction(IReadOnlyList<string> args)
    {
        Action? action = null;

        var i = 0;

        while (i < args.Count)
        {
            var arg = args[i];

            if (arg.Equals("-a", StringComparison.OrdinalIgnoreCase))
                action = args[++i].ParseAction();

            i++;
        }

        action = action switch
        {
            null => Action.Persist,
            Action.Unknown => GetAction(),
            _ => action
        };

        return action!.Value;
    }

    private string? GetBusinessUnit(string businessUnit)
    {
        return _businessUnitMap
            .Where(x => x.Value.Any(y => y.Equals(businessUnit, StringComparison.OrdinalIgnoreCase))).Select(x => x.Key)
            .FirstOrDefault();
    }

    private string GetBusinessUnit(int userId, string? businessUnit)
    {
        if (userId == 14704) //Paul Kwiatkoski
            return "Public Finance";
        
        if (userId == 39664) // Alan Madden
            return "Sovereigns";

        if (userId is 124264 or 143999) // Hank van der Goes and Kolton Shreve
            return "Corporates";

        if (string.IsNullOrWhiteSpace(businessUnit))
            return "Unavailable";

        return GetBusinessUnit(businessUnit) ?? businessUnit;

        /*if (businessUnit.Equals("Aviation, Transportation and Commercial Finance", StringComparison.OrdinalIgnoreCase) ||
            businessUnit.Equals("Aviation, Transportation & Commercial Finance", StringComparison.OrdinalIgnoreCase))
            return "Transportation and Commercial Finance";
        
        if (businessUnit.Equals("ESG", StringComparison.OrdinalIgnoreCase))
            return "Public Finance";
        
        if (businessUnit.Equals("European Head Project Finance & Infrastructure", StringComparison.OrdinalIgnoreCase))
            return "Project Finance & Infrastructure";
        
        if (businessUnit.Equals("Financial Institutions and SRS", StringComparison.OrdinalIgnoreCase))
            return "Financial Institutions";
        
        return businessUnit;*/
    }

    private static int GetBusinessUnitId(IEnumerable<BusinessUnit> businessUnits, IEnumerable<UserInfo> userInfos,
        int analystId)
    {
        var user = userInfos.SingleOrDefault(x => x.Id == analystId);

        if (user == null)
            return 1;

        var businessUnit = businessUnits.SingleOrDefault(x =>
            x.Name.Equals(user.BusinessUnit, StringComparison.OrdinalIgnoreCase));

        return businessUnit?.BusinessUnitId ?? 1;
    }

    private static bool? GetLiborExposure(IEnumerable<LiborExposure> liborExposures, int? workflowId) =>
        liborExposures.FirstOrDefault(x => x.WorkflowId == workflowId)?.IsDecisionPositive;

    private List<LiborExposure> GetLiborExposures() => _thunderballDbContext.ExecSQL<LiborExposure>(@"
        WITH MostRecentTaskLog AS (
            SELECT
                W.workflowId,
                MAX(TL.taskLogId) AS taskLogId
            FROM Workflow W
            INNER JOIN WorkflowTemplate WT on W.workflowTemplateId = WT.workflowTemplateId
            INNER JOIN Stage S on WT.workflowTemplateId = S.workflowTemplateId
            INNER JOIN Task T on S.stageId = T.stageId AND T.taskName = 'LIBOR Exposure'
            INNER JOIN TaskLog TL on T.taskId = TL.taskId AND TL.workflowId = W.workflowId
            INNER JOIN DecisionPointTaskLog DPTL on DPTL.taskLogId = TL.taskLogId
            GROUP BY W.workflowId, S.stageId, T.taskId
        )
        SELECT
            W.workflowId AS WorkflowId,
            DPTL.isDecisionPositive AS IsDecisionPositive
        FROM Workflow W
        INNER JOIN WorkflowTemplate WT on W.workflowTemplateId = WT.workflowTemplateId
        INNER JOIN Stage S on WT.workflowTemplateId = S.workflowTemplateId
        INNER JOIN Task T on S.stageId = T.stageId AND T.taskName = 'LIBOR Exposure'
        LEFT JOIN MostRecentTaskLog on MostRecentTaskLog.workflowId = W.workflowId
        LEFT JOIN DecisionPointTaskLog DPTL on DPTL.taskLogId = MostRecentTaskLog.taskLogId;");
    
    /*private List<LiborExposure> GetLiborExposures() => (from w in _thunderballDbContext.Workflows
        join s in _thunderballDbContext.Stages on w.WorkflowTemplateId equals s.WorkflowTemplateId
        join t in _thunderballDbContext.Tasks on new {StageId = (int?) s.StageId, TaskName = "LIBOR Exposure"}
            equals new
                {t.StageId, t.TaskName}
        join tl in _thunderballDbContext.TaskLogs on new
            {TaskId = (int?) t.TaskId, WorkflowId = (int?) w.WorkflowId, TaskLogType = "decision-point"} equals new
            {tl.TaskId, tl.WorkflowId, tl.TaskLogType} into tlg
        from tlg1 in tlg.DefaultIfEmpty()
        join d in _thunderballDbContext.DecisionPointTaskLogs on tlg1.TaskLogId equals d.TaskLogId into dg
        from dg1 in dg.DefaultIfEmpty()
        select new LiborExposure
        {
            WorkflowId = w.WorkflowId,
            IsDecisionPositive = dg1.IsDecisionPositive
        }).ToList();*/

    private async Task Persist(IEnumerable<CreditEstimate> creditEstimates)
    {
        await using var transaction = await _ratingsHubDbContext.Database.BeginTransactionAsync();

        await _ratingsHubDbContext.Database.ExecuteSqlRawAsync(
            "SET IDENTITY_INSERT [ceca].[CreditEstimate] ON");

        await _ratingsHubDbContext.CreditEstimates.AddRangeAsync(creditEstimates);

        await _ratingsHubDbContext.SaveChangesAsync();

        await _ratingsHubDbContext.Database.ExecuteSqlRawAsync(
            "SET IDENTITY_INSERT [ceca].[CreditEstimate] OFF");

        await transaction.CommitAsync();

        _logger.LogInformation("Persist Completed");
    }

    #endregion
}