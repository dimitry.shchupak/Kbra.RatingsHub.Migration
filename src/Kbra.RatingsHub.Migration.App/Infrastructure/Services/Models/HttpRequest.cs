namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;

public record HttpRequest
{
    #region Public Properties

    public List<KeyValuePair<string, List<string>>>? Headers { get; init; }

    public List<KeyValuePair<string, string?>>? Parameters { get; init; }

    public string RequestUri { get; init; } = null!;

    #endregion
}

public record HttpRequest<T> : HttpRequest
{
    #region Public Properties

    public T? Content { get; init; }

    #endregion
}