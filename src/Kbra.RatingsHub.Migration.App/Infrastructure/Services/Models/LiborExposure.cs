namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;

public record LiborExposure
{
    #region Public Properties

    public bool? IsDecisionPositive { get; init; }

    public int WorkflowId { get; init; }

    #endregion
}