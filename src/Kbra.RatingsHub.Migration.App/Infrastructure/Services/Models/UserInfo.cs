namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;

public record UserInfo
{
    #region Public Properties
    
    public string? BusinessUnit { get; init; }
    
    public string Email { get; init; } = null!;
    
    public string FirstName { get; init; } = null!;

    public int Id { get; init; }
    
    public string LastName { get; init; } = null!;

    #endregion
}