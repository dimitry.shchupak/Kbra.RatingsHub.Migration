using System.Net;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;

public sealed record HttpResponse
{
    #region Public Properties

    public string Content { get; init; } = null!;

    public IEnumerable<KeyValuePair<string, IEnumerable<string>>>? Headers { get; init; }

    public bool IsSuccessStatusCode => (int)StatusCode >= 200 && (int)StatusCode <= 299;

    public HttpStatusCode StatusCode { get; init; }

    #endregion
}