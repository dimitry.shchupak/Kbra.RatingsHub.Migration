namespace Kbra.RatingsHub.Migration.App.Infrastructure.Services.Models;

public sealed record UserServiceResponse<T>
{
    #region Public Properties

    public int StatusCode { get; init; }

    public T? Data { get; init; }

    #endregion
}