using Kbra.RatingsHub.Migration.App.Domain.Entities.RatingsHub;
using Microsoft.EntityFrameworkCore;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Persistence;

public class RatingsHubDbContext : DbContext
{
    #region Public Properties
    
    public virtual DbSet<BusinessUnit> BusinessUnits { get; set; } = null!;

    public virtual DbSet<CreditEstimate> CreditEstimates { get; set; } = null!;
    
    #endregion


    #region Constructors

    public RatingsHubDbContext(DbContextOptions<RatingsHubDbContext> options)
        : base(options)
    {
    }

    #endregion


    #region Protected Methods

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BusinessUnit>(entity =>
        {
            entity.ToTable(nameof(BusinessUnit));
            
            entity.Property(e => e.BusinessUnitId).ValueGeneratedNever();

            entity.HasIndex(e => e.Name).HasDatabaseName("NCUX_BusinessUnit_Name").IsUnique();

            entity.Property(e => e.Name).IsRequired().HasMaxLength(255);
        });
        
        modelBuilder.Entity<CreditEstimate>(entity =>
        {
            entity.ToTable(nameof(CreditEstimate));

            entity.HasIndex(e => e.ActionTypeId);

            entity.HasIndex(e => e.AnalystId);
            
            entity.HasIndex(e => e.BusinessUnitId);
            
            entity.HasIndex(e => e.CreatedByUserId);

            entity.HasIndex(e => e.CreatedByWorkflowId);
            
            entity.HasIndex(e => e.CreditValueId);
            
            entity.HasIndex(e => e.EntityId);

            entity.HasIndex(e => e.RatingOutlookId);

            entity.HasIndex(e => e.UpdatedByUserId);
            
            entity.Property(e => e.CompletedOnDate).HasColumnType("date");

            entity.Property(e => e.SystemEndMomentUtc).HasColumnType("datetime2(4)").ValueGeneratedOnAddOrUpdate()
                .IsConcurrencyToken();

            entity.Property(e => e.SystemStartMomentUtc).HasColumnType("datetime2(4)").ValueGeneratedOnAddOrUpdate();
        });
    }

    #endregion
}