using Kbra.RatingsHub.Migration.App.Domain.Entities.Ratings;
using Microsoft.EntityFrameworkCore;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Persistence;

public class RatingsDbContext : DbContext
{
    #region Public Properties

    public virtual DbSet<CreditEstimate> CreditEstimates { get; set; } = null!;
    
    #endregion


    #region Constructors

    public RatingsDbContext(DbContextOptions<RatingsDbContext> options)
        : base(options)
    {
    }

    #endregion


    #region Protected Methods

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<CreditEstimate>(entity =>
        {
            entity.ToTable(nameof(CreditEstimate));

            entity.HasIndex(e => e.CreditEstimateId);

            entity.HasIndex(e => e.CreditEstimateScaleId);

            entity.HasIndex(e => e.EntityId);

            entity.Property(e => e.CreatedOn).HasColumnType("datetime2(6)");
            
            entity.Property(e => e.CreditEstimateDate).HasColumnType("datetime2(6)");

            entity.Property(e => e.UpdatedOn).HasColumnType("datetime2(6)");
        });
    }

    #endregion
}