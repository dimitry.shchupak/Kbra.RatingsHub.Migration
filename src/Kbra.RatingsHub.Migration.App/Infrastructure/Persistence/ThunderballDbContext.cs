using Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;
using Microsoft.EntityFrameworkCore;
using Task = Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball.Task;

namespace Kbra.RatingsHub.Migration.App.Infrastructure.Persistence;

public class ThunderballDbContext : DbContext
{
    #region Public Properties

    public virtual DbSet<DecisionPointTaskLog> DecisionPointTaskLogs { get; set; } = null!;

    public virtual DbSet<Stage> Stages { get; set; } = null!;

    public virtual DbSet<Task> Tasks { get; set; } = null!;

    public virtual DbSet<TaskLog> TaskLogs { get; set; } = null!;

    public DbSet<Workflow> Workflows { get; set; } = null!;

    #endregion


    #region Constructors

    public ThunderballDbContext(DbContextOptions<ThunderballDbContext> options) : base(options)
    {
    }

    #endregion


    #region Protected Methods

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DecisionPointTaskLog>(entity =>
        {
            entity.ToTable(nameof(DecisionPointTaskLog));
            
            entity.HasKey(e => e.TaskLogId);
        });

        modelBuilder.Entity<Stage>(entity => { entity.ToTable(nameof(Stage)); });

        modelBuilder.Entity<Task>(entity => { entity.ToTable(nameof(Task)); });

        modelBuilder.Entity<TaskLog>(entity => { entity.ToTable(nameof(TaskLog)); });

        modelBuilder.Entity<Workflow>(entity => { entity.ToTable(nameof(Workflow)); });
    }

    #endregion
}