namespace Kbra.RatingsHub.Migration.App.Enumerations;

public enum Action
{
    Unknown = 0,

    Export = 1,

    Persist = 2
}