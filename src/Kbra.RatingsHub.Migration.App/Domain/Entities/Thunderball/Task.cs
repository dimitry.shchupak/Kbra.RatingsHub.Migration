using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;

public record Task : EntityBase
{
    #region Public Properties

    public int? StageId { get; init; }
    
    public int TaskId { get; init; }

    public string TaskName { get; init; } = null!;

    #endregion
}