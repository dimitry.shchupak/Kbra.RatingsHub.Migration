using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;

public record TaskLog : EntityBase
{
    #region Public Properties

    public int? TaskId { get; init; }

    public int TaskLogId { get; init; }

    public string TaskLogType { get; init; } = null!;
    
    public int? WorkflowId { get; init; }

    #endregion
}