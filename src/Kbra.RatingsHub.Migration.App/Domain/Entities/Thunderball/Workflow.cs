using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;

public record Workflow : EntityBase
{
    #region Public Properties

    public int WorkflowId { get; init; }
    
    public int? WorkflowTemplateId { get; init; }

    #endregion
}