using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;

public record DecisionPointTaskLog : EntityBase
{
    #region Public Properties
    
    public bool IsDecisionPositive { get; init; }

    public int TaskLogId { get; init; }

    #endregion
}