using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Thunderball;

public record Stage : EntityBase
{
    #region Public Properties
    
    public int StageId { get; init; }

    public int? WorkflowTemplateId { get; init; }

    #endregion
}