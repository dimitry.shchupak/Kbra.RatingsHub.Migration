using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities;

public record User : EntityBase
{
    #region Public Properties
    
    public string Email { get; init; } = null!;
    
    public string FirstName { get; init; } = null!;

    public int Id { get; init; }
    
    public string LastName { get; init; } = null!;

    #endregion
}