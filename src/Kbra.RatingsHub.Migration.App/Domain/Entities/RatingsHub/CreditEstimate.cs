using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.RatingsHub;

public record CreditEstimate : TemporalEntityBase
{
    #region Public Properties

    public int? ActionTypeId { get; init; }

    public int AnalystId { get; init; }
    
    // Field should be mutable
    public int BusinessUnitId { get; set; }

    public DateTime CompletedOnDate { get; init; }

    public int CreatedByUserId { get; init; }

    public int? CreatedByWorkflowId { get; init; }

    public int CreditEstimateId { get; init; }

    public int CreditValueId { get; init; }

    public int EntityId { get; init; }

    public bool IsActive { get; init; }

    public bool IsCorrection { get; init; }

    public bool IsExpired { get; init; }
    
    // Field should be mutable
    public bool? LiborExposure { get; set; }

    // Field should be mutable
    public int? RatingOutlookId { get; set; }

    #endregion
}