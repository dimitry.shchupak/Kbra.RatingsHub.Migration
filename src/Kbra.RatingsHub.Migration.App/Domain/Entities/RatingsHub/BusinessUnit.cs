using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.RatingsHub;

public record BusinessUnit : EntityBase
{
    #region Public Properties

    public int BusinessUnitId { get; init; }

    public string Name { get; init; } = null!;

    #endregion
}