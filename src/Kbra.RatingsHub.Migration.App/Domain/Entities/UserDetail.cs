using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities;

public record UserDetail : EntityBase
{
    #region Public Properties

    public string BusinessUnit { get; init; } = null!;

    public string Email { get; init; } = null!;

    #endregion
}