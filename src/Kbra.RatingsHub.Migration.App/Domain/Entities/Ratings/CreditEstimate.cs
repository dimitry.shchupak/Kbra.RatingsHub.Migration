using Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Ratings;

public record CreditEstimate : EntityBase
{
    #region Public Properties

    public int? CreatedByUserId { get; init; }

    public DateTime? CreatedOn { get; init; }

    public int? CreatedByWorkflowId { get; init; }

    public DateTime CreditEstimateDate { get; init; }

    public int? CreditEstimateScaleId { get; init; }

    public int CreditEstimateId { get; init; }

    public int? EntityId { get; init; }

    public bool IsActive { get; init; }

    public bool IsCorrection { get; init; }

    public bool IsExpired { get; init; }

    public int? RatingOutlookId { get; init; }

    public int? UpdatedByUserId { get; init; }

    public DateTime? UpdatedOn { get; init; }

    #endregion
}