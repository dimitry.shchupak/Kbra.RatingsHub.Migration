namespace Kbra.RatingsHub.Migration.App.Domain.Entities.Abstract;

public abstract record TemporalEntityBase : EntityBase
{
    #region Public Properties

    public int UpdatedByUserId { get; set; }

    public DateTime SystemEndMomentUtc { get; init; }

    public DateTime SystemStartMomentUtc { get; init; }

    #endregion
}