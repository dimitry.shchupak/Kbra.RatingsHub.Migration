﻿using System.Reflection;
using Kbra.RatingsHub.Migration.App.Infrastructure.Persistence;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services;
using Kbra.RatingsHub.Migration.App.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

Log.Logger = new LoggerConfiguration().WriteTo.Console().Enrich.FromLogContext().CreateBootstrapLogger();

try
{
    Log.Information("Application Starting");

    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
    var configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", false, true);
        
    if (environment == "Development")
        configurationBuilder.AddJsonFile("appsettings.Development.json", true, true);
    
    var configuration = configurationBuilder.AddEnvironmentVariables().Build();

    using var host = Host.CreateDefaultBuilder()
        .ConfigureAppConfiguration((_, builder) => builder.AddConfiguration(configuration)).ConfigureLogging(logging =>
        {
            // Remove any existing registered providers
            logging.ClearProviders();

            logging.AddSerilog();
        }).ConfigureServices((context, services) =>
        {
            services.AddDbContext<RatingsDbContext>(options => options.UseSqlServer(
                context.Configuration.GetConnectionString("RatingsConnection"),
                optionsBuilder => { optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery); }));

            services.AddDbContext<RatingsHubDbContext>(options => options.UseSqlServer(
                context.Configuration.GetConnectionString("RatingsHubConnection"),
                optionsBuilder => { optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery); }));

            services.AddDbContext<ThunderballDbContext>(options => options.UseSqlServer(
                context.Configuration.GetConnectionString("ThunderballConnection"),
                optionsBuilder => { optionsBuilder.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery); }));

            services.AddHttpClient("UserClient", client =>
            {
                client.BaseAddress = new Uri(context.Configuration.GetValue<string>("Clients:UserClient:BaseUri"));
                client.DefaultRequestHeaders.Add("X-API-KEY", context.Configuration.GetValue<string>("Clients:UserClient:ApiKey"));
            });

            services.AddSingleton<IApplicationService, ApplicationService>();
            services.AddScoped(_ => new LdapService(context.Configuration.GetValue<string>("Services:LdapService:Username"), context.Configuration.GetValue<string>("Services:LdapService:Password")));
            services.AddScoped<IUserService, UserService>();
        }).Build();

    var applicationService = ActivatorUtilities.CreateInstance<ApplicationService>(host.Services);
    
    await applicationService.Run(args);
}
catch (Exception exception)
{
    Log.Fatal(exception, "ApplicationName: {ApplicationName}", Assembly.GetExecutingAssembly().GetName().Name);
}
finally
{
    Log.CloseAndFlush();
}