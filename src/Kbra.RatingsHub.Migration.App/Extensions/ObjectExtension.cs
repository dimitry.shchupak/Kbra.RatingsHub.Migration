using Kbra.RatingsHub.Migration.App.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Kbra.RatingsHub.Migration.App.Extensions;

public static class ObjectExtension
{
    #region Public Methods

    public static string Serialize(this object item, bool withDefaults = true) => item.Serialize(withDefaults ? GetJsonSerializerSettings() : null);

    public static string Serialize(this object item, JsonSerializerSettings? settings) => JsonConvert.SerializeObject(item, settings);

    #endregion


    #region Private Methods

    private static JsonSerializerSettings GetJsonSerializerSettings()
    {
        var settings = new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new PiecewiseNamingStrategy(new CamelCaseNamingStrategy())
                {
                    OverrideSpecifiedNames = true,
                    ProcessDictionaryKeys = true
                }
            },
            NullValueHandling = NullValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        settings.Converters.Add(new StringEnumConverter());

        return settings;
    }

    #endregion
}