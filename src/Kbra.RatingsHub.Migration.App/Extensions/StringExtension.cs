using Newtonsoft.Json;
using Action = Kbra.RatingsHub.Migration.App.Enumerations.Action;

namespace Kbra.RatingsHub.Migration.App.Extensions;

public static class StringExtension
{
    #region Public Methods

    public static T? Deserialize<T>(this string item) => JsonConvert.DeserializeObject<T>(item);

    public static Action? ParseAction(this string item) => item.ParseEnum<Action>(Action.Unknown, true);

    public static T? ParseEnum<T>(this string item, T? defaultValue = null, bool ignoreCase = false)
        where T : struct, IComparable, IFormattable, IConvertible => string.IsNullOrWhiteSpace(item) ||
                                                                     !typeof(T).IsEnum ||
                                                                     !Enum.TryParse<T>(item, ignoreCase,
                                                                         out var value) ||
                                                                     !Enum.IsDefined(typeof(T), value)
        ? defaultValue
        : value;

    #endregion
}