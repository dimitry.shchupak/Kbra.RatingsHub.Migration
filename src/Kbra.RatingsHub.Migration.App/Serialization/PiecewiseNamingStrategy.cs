using Newtonsoft.Json.Serialization;

namespace Kbra.RatingsHub.Migration.App.Serialization;

public class PiecewiseNamingStrategy : NamingStrategy
{
    #region Private Fields

    private readonly NamingStrategy _baseStrategy;

    #endregion


    #region Constructors

    public PiecewiseNamingStrategy(NamingStrategy baseStrategy) => _baseStrategy = baseStrategy ?? throw new ArgumentNullException(nameof(baseStrategy));

    #endregion


    #region Protected Methods

    protected override string ResolvePropertyName(string name) => string.Join(".", name.Split(".").Select(x => _baseStrategy.GetPropertyName(x, false)));

    #endregion
}